﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetAbstrac3616801211.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity657456673.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE2149396216.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Vufor3491240575.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Stora3897282321.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle4061728485.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle3506117492.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaMacros1884408435.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager2424874861.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra1329355276.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer2933102835.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Fp1598668988.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4106934884.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4137084396.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec829768013.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi2617831468.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Ren804170727.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil3083157244.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil1916387570.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceUtilities4096327849.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder1347637805.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_InitStat4409649.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Updat1473252352.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Filte3082493643.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe1958726506.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBe2386081773.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTracker89845299.h"
#include "Vuforia_UnityExtensions_Vuforia_SimpleTargetData3993525265.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour3993660444.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSource2832298792.h"
#include "Vuforia_UnityExtensions_Vuforia_Tracker189438242.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager308318605.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeh4084926705.h"
#include "Vuforia_UnityExtensions_Vuforia_UserDefinedTargetB3589690572.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst395384314.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundMan3515346924.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton3703236737.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton_Sens1678924861.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2478279366.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamARController2804466264.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManager1585193471.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult1915507197.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode695600879.h"
#include "Vuforia_UnityExtensions_Vuforia_WordList1278495262.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration2396922556.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibrat626398268.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe1486305137.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDet978476007.h"
#include "Vuforia_UnityExtensions_iOS_U3CModuleU3E3783534214.h"
#include "Vuforia_UnityExtensions_iOS_Vuforia_VuforiaNativeI1210651633.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour2431285219.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3077176941.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro965510117.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH870608571.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan1082256726.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler3809113141.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour3495034315.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer852788525.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer3656371703.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen1383853028.h"
#include "AssemblyU2DCSharp_Vuforia_WSAUnityPlayer425981959.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2994129365.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour3504654311.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour3836044259.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour966064926.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour4009935945.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTarget2111803406.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour2405314212.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour3400239837.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour3058161409.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour584991835.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin4184040062.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper556656694.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3161817952.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2515041812.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour2060629989.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaConfiguration3823746026.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaRuntimeInitializa1850075444.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (MultiTargetAbstractBehaviour_t3616801211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[1] = 
{
	MultiTargetAbstractBehaviour_t3616801211::get_offset_of_mMultiTarget_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (VuforiaUnity_t657456673), -1, sizeof(VuforiaUnity_t657456673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1701[1] = 
{
	VuforiaUnity_t657456673_StaticFields::get_offset_of_mHoloLensApiAbstraction_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (InitError_t2149396216)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1702[12] = 
{
	InitError_t2149396216::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (VuforiaHint_t3491240575)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1703[4] = 
{
	VuforiaHint_t3491240575::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (StorageType_t3897282321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1704[4] = 
{
	StorageType_t3897282321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (VuforiaARController_t4061728485), -1, sizeof(VuforiaARController_t4061728485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1705[39] = 
{
	VuforiaARController_t4061728485::get_offset_of_CameraDeviceModeSetting_1(),
	VuforiaARController_t4061728485::get_offset_of_MaxSimultaneousImageTargets_2(),
	VuforiaARController_t4061728485::get_offset_of_MaxSimultaneousObjectTargets_3(),
	VuforiaARController_t4061728485::get_offset_of_UseDelayedLoadingObjectTargets_4(),
	VuforiaARController_t4061728485::get_offset_of_CameraDirection_5(),
	VuforiaARController_t4061728485::get_offset_of_MirrorVideoBackground_6(),
	VuforiaARController_t4061728485::get_offset_of_mWorldCenterMode_7(),
	VuforiaARController_t4061728485::get_offset_of_mWorldCenter_8(),
	VuforiaARController_t4061728485::get_offset_of_mTrackerEventHandlers_9(),
	VuforiaARController_t4061728485::get_offset_of_mVideoBgEventHandlers_10(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaInitialized_11(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaStarted_12(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaDeinitialized_13(),
	VuforiaARController_t4061728485::get_offset_of_mOnTrackablesUpdated_14(),
	VuforiaARController_t4061728485::get_offset_of_mRenderOnUpdate_15(),
	VuforiaARController_t4061728485::get_offset_of_mOnPause_16(),
	VuforiaARController_t4061728485::get_offset_of_mPaused_17(),
	VuforiaARController_t4061728485::get_offset_of_mOnBackgroundTextureChanged_18(),
	VuforiaARController_t4061728485::get_offset_of_mStartHasBeenInvoked_19(),
	VuforiaARController_t4061728485::get_offset_of_mHasStarted_20(),
	VuforiaARController_t4061728485::get_offset_of_mBackgroundTextureHasChanged_21(),
	VuforiaARController_t4061728485::get_offset_of_mCameraConfiguration_22(),
	VuforiaARController_t4061728485::get_offset_of_mEyewearBehaviour_23(),
	VuforiaARController_t4061728485::get_offset_of_mVideoBackgroundMgr_24(),
	VuforiaARController_t4061728485::get_offset_of_mCheckStopCamera_25(),
	VuforiaARController_t4061728485::get_offset_of_mClearMaterial_26(),
	VuforiaARController_t4061728485::get_offset_of_mMetalRendering_27(),
	VuforiaARController_t4061728485::get_offset_of_mHasStartedOnce_28(),
	VuforiaARController_t4061728485::get_offset_of_mWasEnabledBeforePause_29(),
	VuforiaARController_t4061728485::get_offset_of_mObjectTrackerWasActiveBeforePause_30(),
	VuforiaARController_t4061728485::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_31(),
	VuforiaARController_t4061728485::get_offset_of_mLastUpdatedFrame_32(),
	VuforiaARController_t4061728485::get_offset_of_mTrackersRequestedToDeinit_33(),
	VuforiaARController_t4061728485::get_offset_of_mMissedToApplyLeftProjectionMatrix_34(),
	VuforiaARController_t4061728485::get_offset_of_mMissedToApplyRightProjectionMatrix_35(),
	VuforiaARController_t4061728485::get_offset_of_mLeftProjectMatrixToApply_36(),
	VuforiaARController_t4061728485::get_offset_of_mRightProjectMatrixToApply_37(),
	VuforiaARController_t4061728485_StaticFields::get_offset_of_mInstance_38(),
	VuforiaARController_t4061728485_StaticFields::get_offset_of_mPadlock_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (WorldCenterMode_t3506117492)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1706[5] = 
{
	WorldCenterMode_t3506117492::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (VuforiaMacros_t1884408435)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t1884408435 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1707[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (VuforiaManager_t2424874861), -1, sizeof(VuforiaManager_t2424874861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1708[1] = 
{
	VuforiaManager_t2424874861_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (TrackableIdPair_t1329355276)+ sizeof (Il2CppObject), sizeof(TrackableIdPair_t1329355276 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1709[2] = 
{
	TrackableIdPair_t1329355276::get_offset_of_TrackableId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableIdPair_t1329355276::get_offset_of_ResultId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (VuforiaRenderer_t2933102835), -1, sizeof(VuforiaRenderer_t2933102835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1710[1] = 
{
	VuforiaRenderer_t2933102835_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (FpsHint_t1598668988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1711[5] = 
{
	FpsHint_t1598668988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (VideoBackgroundReflection_t4106934884)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1712[4] = 
{
	VideoBackgroundReflection_t4106934884::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (VideoBGCfgData_t4137084396)+ sizeof (Il2CppObject), sizeof(VideoBGCfgData_t4137084396 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1713[4] = 
{
	VideoBGCfgData_t4137084396::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_enabled_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_reflectionInteger_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (Vec2I_t829768013)+ sizeof (Il2CppObject), sizeof(Vec2I_t829768013 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1714[2] = 
{
	Vec2I_t829768013::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vec2I_t829768013::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (VideoTextureInfo_t2617831468)+ sizeof (Il2CppObject), sizeof(VideoTextureInfo_t2617831468 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1715[2] = 
{
	VideoTextureInfo_t2617831468::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoTextureInfo_t2617831468::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (RendererAPI_t804170727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1716[5] = 
{
	RendererAPI_t804170727::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (VuforiaRuntimeUtilities_t3083157244), -1, sizeof(VuforiaRuntimeUtilities_t3083157244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1717[2] = 
{
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (InitializableBool_t1916387570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1718[4] = 
{
	InitializableBool_t1916387570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (SurfaceUtilities_t4096327849), -1, sizeof(SurfaceUtilities_t4096327849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1719[1] = 
{
	SurfaceUtilities_t4096327849_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (TargetFinder_t1347637805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (InitState_t4409649)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1721[6] = 
{
	InitState_t4409649::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (UpdateState_t1473252352)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1722[12] = 
{
	UpdateState_t1473252352::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (FilterMode_t3082493643)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1723[3] = 
{
	FilterMode_t3082493643::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (TargetSearchResult_t1958726506)+ sizeof (Il2CppObject), sizeof(TargetSearchResult_t1958726506_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1724[6] = 
{
	TargetSearchResult_t1958726506::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (TextRecoAbstractBehaviour_t2386081773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[12] = 
{
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (TextTracker_t89845299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (SimpleTargetData_t3993525265)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t3993525265 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1727[2] = 
{
	SimpleTargetData_t3993525265::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimpleTargetData_t3993525265::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (TrackableBehaviour_t1779888572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[8] = 
{
	TrackableBehaviour_t1779888572::get_offset_of_U3CTimeStampU3Ek__BackingField_2(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableName_3(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreserveChildSize_4(),
	TrackableBehaviour_t1779888572::get_offset_of_mInitializedInEditor_5(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreviousScale_6(),
	TrackableBehaviour_t1779888572::get_offset_of_mStatus_7(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackable_8(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableEventHandlers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (Status_t4057911311)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1730[7] = 
{
	Status_t4057911311::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (CoordinateSystem_t3993660444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1731[4] = 
{
	CoordinateSystem_t3993660444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (TrackableSource_t2832298792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (Tracker_t189438242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[1] = 
{
	Tracker_t189438242::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (TrackerManager_t308318605), -1, sizeof(TrackerManager_t308318605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1734[1] = 
{
	TrackerManager_t308318605_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (TurnOffAbstractBehaviour_t4084926705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t3589690572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (VideoBackgroundAbstractBehaviour_t395384314), -1, sizeof(VideoBackgroundAbstractBehaviour_t395384314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1737[12] = 
{
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaARController_4(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mFrameCounter_8(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mRenderCounter_9(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mResetMatrix_10(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaFrustumSkew_11(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCenterToEyeAxis_12(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mDisabledMeshRenderers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (VideoBackgroundManager_t3515346924), -1, sizeof(VideoBackgroundManager_t3515346924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1738[8] = 
{
	VideoBackgroundManager_t3515346924::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_t3515346924::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_t3515346924::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_t3515346924::get_offset_of_mTexture_4(),
	VideoBackgroundManager_t3515346924::get_offset_of_mVideoBgConfigChanged_5(),
	VideoBackgroundManager_t3515346924::get_offset_of_mNativeTexturePtr_6(),
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_mPadlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (VirtualButton_t3703236737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (Sensitivity_t1678924861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1740[4] = 
{
	Sensitivity_t1678924861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (VirtualButtonAbstractBehaviour_t2478279366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[15] = 
{
	0,
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviousSensitivity_9(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviouslyEnabled_10(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPressed_11(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHandlers_12(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mLeftTop_13(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mRightBottom_14(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mUnregisterOnDestroy_15(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mVirtualButton_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (WebCamARController_t2804466264), -1, sizeof(WebCamARController_t2804466264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1742[6] = 
{
	WebCamARController_t2804466264::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t2804466264::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t2804466264::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t2804466264::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t2804466264_StaticFields::get_offset_of_mInstance_5(),
	WebCamARController_t2804466264_StaticFields::get_offset_of_mPadlock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (WordManager_t1585193471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (WordResult_t1915507197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (WordTemplateMode_t1097144495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1746[3] = 
{
	WordTemplateMode_t1097144495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (WordAbstractBehaviour_t2878458725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[3] = 
{
	WordAbstractBehaviour_t2878458725::get_offset_of_mMode_10(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mSpecificWord_11(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mWord_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (WordFilterMode_t695600879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1748[4] = 
{
	WordFilterMode_t695600879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (WordList_t1278495262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (EyewearCalibrationProfileManager_t2396922556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (EyewearUserCalibrator_t626398268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1752[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (__StaticArrayInitTypeSizeU3D24_t978476007)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t978476007 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (VuforiaNativeIosWrapper_t1210651633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1758[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (BackgroundPlaneBehaviour_t2431285219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (CloudRecoBehaviour_t3077176941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (CylinderTargetBehaviour_t2091399712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (DefaultInitializationErrorHandler_t965510117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[3] = 
{
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorText_2(),
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (DefaultSmartTerrainEventHandler_t870608571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[3] = 
{
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (DefaultTrackableEventHandler_t1082256726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[1] = 
{
	DefaultTrackableEventHandler_t1082256726::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (GLErrorHandler_t3809113141), -1, sizeof(GLErrorHandler_t3809113141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1768[3] = 
{
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorText_2(),
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (HideExcessAreaBehaviour_t3495034315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (ImageTargetBehaviour_t2654589389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (AndroidUnityPlayer_t852788525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t852788525::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t852788525::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (ComponentFactoryStarterBehaviour_t3249343815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (IOSUnityPlayer_t3656371703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[1] = 
{
	IOSUnityPlayer_t3656371703::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (VuforiaBehaviourComponentFactory_t1383853028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (WSAUnityPlayer_t425981959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[1] = 
{
	WSAUnityPlayer_t425981959::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (MaskOutBehaviour_t2994129365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (MultiTargetBehaviour_t3504654311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (ObjectTargetBehaviour_t3836044259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (PropBehaviour_t966064926), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (ReconstructionBehaviour_t4009935945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (ReconstructionFromTargetBehaviour_t2111803406), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (SurfaceBehaviour_t2405314212), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (TextRecoBehaviour_t3400239837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (TurnOffBehaviour_t3058161409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (TurnOffWordBehaviour_t584991835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (UserDefinedTargetBuildingBehaviour_t4184040062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (VRIntegrationHelper_t556656694), -1, sizeof(VRIntegrationHelper_t556656694_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1787[12] = 
{
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraMatrixOriginal_2(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraMatrixOriginal_3(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCamera_4(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCamera_5(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftExcessAreaBehaviour_6(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightExcessAreaBehaviour_7(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraPixelRect_8(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraPixelRect_9(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraDataAcquired_10(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraDataAcquired_11(),
	VRIntegrationHelper_t556656694::get_offset_of_IsLeft_12(),
	VRIntegrationHelper_t556656694::get_offset_of_TrackableParent_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (VideoBackgroundBehaviour_t3161817952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (VirtualButtonBehaviour_t2515041812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (VuMarkBehaviour_t2060629989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (VuforiaBehaviour_t359035403), -1, sizeof(VuforiaBehaviour_t359035403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1791[1] = 
{
	VuforiaBehaviour_t359035403_StaticFields::get_offset_of_mVuforiaBehaviour_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (VuforiaConfiguration_t3823746026), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (VuforiaRuntimeInitialization_t1850075444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (WireframeBehaviour_t2494532455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[4] = 
{
	WireframeBehaviour_t2494532455::get_offset_of_lineMaterial_2(),
	WireframeBehaviour_t2494532455::get_offset_of_ShowLines_3(),
	WireframeBehaviour_t2494532455::get_offset_of_LineColor_4(),
	WireframeBehaviour_t2494532455::get_offset_of_mLineMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (WireframeTrackableEventHandler_t1535150527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[1] = 
{
	WireframeTrackableEventHandler_t1535150527::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (WordBehaviour_t3366478421), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
